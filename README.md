# Instalace Arch linux
Stažení archlinux.org, přidaní na flash drive pomocí Etcher
## Příprava
Nastavení české klávesnice
``` bash
loadkeys cz-qwertz
```

Rozdělení disku
``` bash
fdisk -l
fdisk /dev/sda
```
Vybrat partition pomocí 'd' vymazat vše. Poté pomocí 'n' vytvořit novou, nejprve se vytváří EFI partition. Number 1, enter, +512M, t, L a najít EFI partition.
potě vytvořit druhou partition se zbytkem místa, pomocí 't' najít Linux FileSystem.

Vytvoření filesystemu
``` bash
mkfs.fat -F32 /dev/sda1  -- EFI
mkfs.ext4 /dev/sda2 --root a ostatní
```
Připojení k internetu 
``` bash
iwctl
ping google.com
```
Nastavení českého mirroru
``` bash
pacman -Syy
pacman -S reflector
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak
reflector -c "CZ" -f 12 -l 10 -n 12 --save /etc/pacman.d/mirrorlist
```
## Vlastní instalace
``` bash
mount /dev/sda2 /mnt  -- záleží co kam chceš namountovat
mkdir /mnt/home 
mount /dev/sda3 /mnt/home

pacstrap /mnt base base-devel linux linux-firmware  nano git wget
```
Vytvoření fstabulky
``` bash
genfstab -U /mnt >> /mnt/etc/fstab
```
přepnutí na chroot
``` bash
arch-chroot /mnt
```
nastavení TimeZone a locale
``` bash
timedatectl set-timezone Europe/Prague
timedatectl set-ntp true
hwclock --systohc
nano /etc/locale.gen -- odkomentovat cs_CZ.UTF-8
locale-gen
echo LANG=cs_CZ.UTF-8 > /etc/locale.conf
export LANG=cs_CZ.UTF-8
```
Nastavení sítě
``` bash
echo jiri > /etc/hostname
touch /etc/hosts
nano /etc/hosts

127.0.0.1	localhost
::1		localhost
127.0.1.1	jiri
```
Nastavení root password
``` bash
passwd 
```
Instalace GRUB
``` bash
pacman -S grub efibootmgr
mkdir /boot/efi
mount /dev/sda1 /boot/efi
grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi
grub-mkconfig -o /boot/grub/grub.cfg
```
Instalace GNOME
``` bash
pacman -S xorg xf86-video-intel
pacman -S gnome
pacman -S gnome-extra
pacman -S gnome-shell-extensions
pacman -S gnome-tweaks

systemctl start gdm.service
systemctl enable gdm.service
systemctl enable NetworkManager.service
```
Vytvoření uživatele
``` bash
useradd -c 'Jiří' -m -G tty,ftp,mai,pro,http,games,network,power,vboxusers,docker,git,video,optical,input,audio,wheel -s /bin/bash -U jiri
passwd jiri
```
přesun do archu
``` bash
exit
shutdown now
```
Vytáhnout flash drive a nabootovat PC
## Čerstvá isntalace

Nastavení správného fontu console
``` bash
setfont eurlatgr
```
Nastavení visudo
``` bash
EDITOR=nano visudo

## Uncomment to allow members of group wheel to execute any command
%wheel ALL=(ALL) ALL

Defaults passwd_timeout=0
Defaults passprompt="^G[sudo] password for %p: "
Defaults timestamp_timeout=10
Defaults editor=/usr/bin/nano, !env_editor
Defaults env_keep += "ftp_proxy http_proxy https_proxy no_proxy"
```
Instalované aplikace pacman -S
``` bash
code, docker,bash_completion,acpi,virtualbox,virtualbox-host-modules-arch,hdparm,networkmanager-openconnect,gtk3,ttf-dejavu,arc-gtk-theme,xcursor-vanilla-dmz,keepass,
gnome-online-accounts,evolution-ews,ntp,vlc,subversion,virtualbox-guest-iso,gimp,jre8-openjdk,ntfs-3g,htop,cups,cups-pdf,samba,avahi,system-config-printer,python-pysmbc,acpid,aspell
```
Instalace yay
``` bash
cd /opt
git clone https://github.com/Jguer/yay.git
cd yay
makepkg -si
```
Aktivace senzorů
``` bash
sensors-detect
pwmconfig
fancontrol
```

Aplikace přes yay, uživatelské balíčky
``` bash
teams,skypeforlinux,proxyman-git,intellij-idea-ultimate-edition,intellij-idea-ultimate-edition-jre,google-chrome,systemd-numlockontty, virtualbox-ext-oracle,dbeaver,postman-bin,oracle-sqldeveloper,freeoffice, aspell-cs
```
Aktivovat service
``` bash
docker.service,docker.socket, numLockOnTty,fstrim.timer,org.cups.cupsd.service,avahi-daemon.service,acpid.service,smb.service
```
Pro SAMBu je potřeba nastavit smb.conf do /etc/samba - lze nalézt výchozí na wiki

zrychlení bootu přes GRUP
``` bash
nano /etc/default/grub

GRUB_FORCE_HIDDEN_MENU="true"
touch /etc/grub.d/31_hold_shift
nano /etc/grub.d/31_hold_shift

 zkopírovat content  https://gist.githubusercontent.com/anonymous/8eb2019db2e278ba99be/raw/257f15100fd46aeeb8e33a7629b209d0a14b9975/gistfile1.sh

chmod a+x /etc/grub.d/31_hold_shift
grub-mkconfig -o /boot/grub/grub.cfg
```
Nastavení virtualBoxu
``` bash
modprobe vboxdrv
modprobe vboxnetflt
modprobe vboxnetadp
```

Nastavení klávesové zkratky na změnu jazyka přes 'tweaks', nastavení klávesové zkratky na terminál v Nastavení.V 'tweaks' změnit Vzhled, Záhlaví oken - minimalizace, maximalizace

SQLdeveloper je nutné stáhnout z oracle a poté přesunout do ~/.cache/yay/oracle-sqldeveloper a spustit příkaz ve složce:
``` bash
makepkg -si
```

Nastavení proxyman
``` bash
proxyman set
# vyplnit proxy a uložit pod profilem, poté stačí:
proxyman load <profile>
1
# vypnutí proxy
proxyman unset <profile>
1
```


Nastavení tiskárny přes: system-control-printer

smb://ntpsw403.to2.to2cz.cz/TPARMASCOLOR

[1] https://wiki.archlinux.org/index.php/Installation_guide  
[2] https://itsfoss.com/install-arch-linux/

